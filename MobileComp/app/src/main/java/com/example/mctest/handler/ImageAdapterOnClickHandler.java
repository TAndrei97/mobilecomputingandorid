package com.example.mctest.handler;

import android.graphics.Bitmap;
import android.view.Menu;

import com.example.mctest.utilities.ImageGalleryAdapter;

public interface ImageAdapterOnClickHandler {

    void setImageGalleryAdapter(ImageGalleryAdapter mImageGalleryAdapter);

    void setMenu(Menu menu);

    void onClick(Bitmap selectedImage, ImageGalleryAdapter.ImageGalleryAdapterViewHolder imageGalleryAdapterViewHolder);

    void onLongClick(Bitmap selectedImage);

    void removeImages();
}
