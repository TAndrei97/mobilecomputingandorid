package com.example.mctest;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mctest.databinding.ActivityAccomodationsListBinding;
import com.example.mctest.model.Accomodation;
import com.example.mctest.utilities.AccomodationDetailsAdapter;
import com.example.mctest.utilities.ProgressDialogUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AccomodationsListActivity extends AppCompatActivity {

    private ActivityAccomodationsListBinding mBinding;
    private AccomodationDetailsAdapter mAccomodationDetailsAdapter;
    private List<Accomodation> mAccomodations;
    private String url;
    private int userId;

    private ProgressDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accomodations_list);
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.session_preferences), Context.MODE_PRIVATE);
        userId = sharedPreferences.getInt(getString(R.string.userId), 0);

        url = getString(R.string.url) + getString(R.string.accomodation_uri) + "/all/" +userId;
        mAccomodationDetailsAdapter = new AccomodationDetailsAdapter();

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_accomodations_list);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mBinding.recyclerviewAccomodations.setLayoutManager(layoutManager);
        mBinding.recyclerviewAccomodations.setAdapter(mAccomodationDetailsAdapter);
        mBinding.recyclerviewAccomodations.setNestedScrollingEnabled(false);
        ProgressDialogUtil.start(this);

        getAllAccomodations();
    }

    private void getAllAccomodations() {
        RequestQueue MyRequestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        mAccomodations = new ArrayList<>();
                        for(int i=0; i < response.length(); i++) {
                            JSONObject jsonobject = null;
                            try {
                                jsonobject = response.getJSONObject(i);
                                Integer id       = jsonobject.getInt("id");
                                String name    = jsonobject.getString("name");
                                String description  = jsonobject.getString("description");
                                String phone = jsonobject.getString("phone");
                                String address = jsonobject.getString("address");
                                Accomodation accomodation = new Accomodation(id, name, description, phone, address);
                                mAccomodations.add(accomodation);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        mAccomodationDetailsAdapter.setAccomodations(mAccomodations);
                        ProgressDialogUtil.stop(AccomodationsListActivity.this);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(AccomodationsListActivity.this, "Unknown user", Toast.LENGTH_SHORT).show();
                        error.printStackTrace();
                        Log.d("Error.Response", "test");
                    }
                }
        );
        MyRequestQueue.add(getRequest);

    }


}
