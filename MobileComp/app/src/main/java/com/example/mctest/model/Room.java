package com.example.mctest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Room {

    private Integer quantity;
    private Integer capacity;
    private Integer price;

}
