package com.example.mctest;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.mctest.databinding.ActivityMainBinding;
import com.example.mctest.utilities.AccomodationDetailsAdapter;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        mBinding.mainProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openProfileActivity();
            }
        });

        mBinding.mainNewAccomodationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNewAccomodationActivity();
            }
        });

        mBinding.mainViewAccomodationsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openViewAccomodationsActivity();
            }
        });

        mBinding.mainLogoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    private void logout() {
        Intent intent = new Intent(MainActivity.this, LoginPageActivity.class);
        startActivity(intent);
    }

    private void openViewAccomodationsActivity() {
        Intent intent = new Intent(MainActivity.this, AccomodationsListActivity.class);
        startActivity(intent);
    }

    private void openNewAccomodationActivity() {
        Intent intent = new Intent(MainActivity.this, InsertAccommodationActivity.class);
        startActivity(intent) ;
    }


    private void openProfileActivity() {
        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
        startActivity(intent);
    }
}
