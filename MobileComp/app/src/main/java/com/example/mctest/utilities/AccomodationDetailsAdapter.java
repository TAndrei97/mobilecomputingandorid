package com.example.mctest.utilities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mctest.R;
import com.example.mctest.ViewAccomodationActivity;
import com.example.mctest.model.Accomodation;

import java.util.List;

public class AccomodationDetailsAdapter extends RecyclerView.Adapter<AccomodationDetailsAdapter.AccomodationDetailsAdapterViewHolder> {

    private List<Accomodation> mAccomodations;

    @NonNull
    @Override
    public AccomodationDetailsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.accomodation_viev_details;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        return new AccomodationDetailsAdapter.AccomodationDetailsAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AccomodationDetailsAdapterViewHolder accomodationDetailsAdapterViewHolder, int i) {
        Accomodation accomodation = mAccomodations.get(i);
        accomodationDetailsAdapterViewHolder.mAccomodationNameTF.setText(accomodation.getName());
        accomodationDetailsAdapterViewHolder.mAccomodation = accomodation;
    }

    @Override
    public int getItemCount() {
        if (null == mAccomodations) {
            return 0;
        }
        return mAccomodations.size();
    }

    public void setAccomodations(List<Accomodation> accomodations){
        this.mAccomodations = accomodations;
        notifyDataSetChanged();
    }

    public class AccomodationDetailsAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView mAccomodationNameTF;
        private Accomodation mAccomodation;
        private LinearLayout mLayout;

        public AccomodationDetailsAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            mAccomodationNameTF = (TextView) itemView.findViewById(R.id.listAccomodationDetailTV);
            mLayout = (LinearLayout)itemView.findViewById(R.id.listAccomodationDetailLL);
            mLayout.setOnClickListener(this);
            mAccomodationNameTF.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), ViewAccomodationActivity.class);
            intent.putExtra(v.getContext().getString(R.string.selected_accomodation), mAccomodation.getId());
            v.getContext().startActivity(intent);
        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }

    }
}
