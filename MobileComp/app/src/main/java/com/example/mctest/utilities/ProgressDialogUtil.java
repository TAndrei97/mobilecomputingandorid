package com.example.mctest.utilities;

import android.app.ProgressDialog;
import android.content.Context;

public class ProgressDialogUtil {
    private static ProgressDialog dialog;

    public static void start(Context context) {
        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        // Set the progress dialog title and message
        dialog.setTitle("Title of progress dialog.");
        dialog.setMessage("Loading.........");
        dialog.show();
    }

    public static void stop(Context context) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }



}
