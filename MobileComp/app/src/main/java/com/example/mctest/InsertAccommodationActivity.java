package com.example.mctest;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.databinding.DataBindingUtil;

import com.example.mctest.databinding.ActivityInsertAccommodationBinding;
import com.example.mctest.utilities.CustomMarker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class InsertAccommodationActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    private static final String TAG = InsertAccommodationActivity.class.getSimpleName();

    private static final long LOCATION_REFRESH_TIME = 5000;
    private static final float LOCATION_REFRESH_DISTANCE = 500;
    private static final int REQUEST_LOCATION = 1;

    private MapView mMapView;
    private ActivityInsertAccommodationBinding mBinding = null;
    private GoogleMap mGoogleMap;
    private LocationManager locationManager;
    private String lastKnownAddress;
    private Location lastKnownLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_accommodation);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_insert_accommodation);
        mMapView = mBinding.map;

        if (mMapView != null) {
            mMapView.onCreate(savedInstanceState);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }

        mBinding.insertAccomodationNextBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                openAccomodationDetailsActivity();
            }
        });

        mBinding.editTextAddress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus == false) {
                    placeMarker(mBinding.editTextAddress.getText().toString());
                }
            }
        });

        mBinding.setCurrentLocationAsAddressBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.editTextAddress.setText(lastKnownAddress);
                placeMarker(lastKnownAddress);
            }
        });
        getLocation();

    }

    private void openAccomodationDetailsActivity() {
        Intent intent = new Intent(InsertAccommodationActivity.this,
                InsertAccomodationDetailsActivity.class);
        intent.putExtra(getString(R.string.accomodation_name), mBinding.editTextName.getText().toString());
        intent.putExtra(getString(R.string.accomodation_description), mBinding.editTextDescription.getText().toString());
        intent.putExtra(getString(R.string.accomodation_phone), mBinding.editTextPhone.getText().toString());
        intent.putExtra(getString(R.string.accomodation_address), mBinding.editTextAddress.getText().toString());
        startActivity(intent);
    }

    private void placeMarker(String address) {
        if (mGoogleMap == null) {
            return;
        }
        LatLng latLngAddress = getLocationFromAddress(address);

        //TODO: add wifi request
        if (latLngAddress == null) {
            return;
        }
        mGoogleMap.clear();
        MarkerOptions marker = new MarkerOptions().position(latLngAddress);
        marker.icon(CustomMarker.bitmapDescriptorFromVector(this, R.drawable.ic_travel_transport_hotel_vacation_holidays_tourist_tourism_travelling_traveling_147_512));

        mGoogleMap.addMarker(marker);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLngAddress));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }


    private void getLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10f, this);
        lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        computeAddress(lastKnownLocation);
    }

    public LatLng getLocationFromAddress(String address) {
        Geocoder coder = new Geocoder(this);
        List<Address> addressList;
        LatLng p1 = null;

        try {
            addressList = coder.getFromLocationName(address, 5);
            if (addressList == null) {
                return null;
            }
            Address location = addressList.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return p1;

    }

    private void computeAddress(Location location) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        if (location == null) {
            return;
        }
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            lastKnownAddress = addresses.get(0).getAddressLine(0);
            displayCurrentAddress(lastKnownAddress);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void displayCurrentAddress(String address) {
        mBinding.currentAddress.setText(address);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // We can now safely use the API we requested access to
                Toast.makeText(this, "granted", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "denied", Toast.LENGTH_LONG).show();
            }
        }
    }


    private void populateLocationInfo(String location) {
        mBinding.editTextAddress.setText(location);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getBaseContext());
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setMyLocationEnabled(true);
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }

    @Override
    public void onLocationChanged(Location location) {
        //mBinding.editTextAddress.setText(location.toString());
        computeAddress(location);

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }
}
