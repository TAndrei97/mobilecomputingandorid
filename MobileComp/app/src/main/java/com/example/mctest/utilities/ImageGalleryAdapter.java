package com.example.mctest.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.mctest.R;
import com.example.mctest.handler.ImageAdapterOnClickHandler;
import com.example.mctest.handler.ImageGalleryHandler;

import java.util.List;

public class ImageGalleryAdapter extends RecyclerView.Adapter<ImageGalleryAdapter.ImageGalleryAdapterViewHolder> {
    private List<Bitmap> mImages;
    private ImageAdapterOnClickHandler mClickHandler;

    public ImageGalleryAdapter(ImageAdapterOnClickHandler clickHandler) {
        this.mClickHandler = clickHandler;
    }

    @NonNull
    @Override
    public ImageGalleryAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.photo_gallery_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        return new ImageGalleryAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageGalleryAdapterViewHolder imageGalleryAdapterViewHolder, int i) {
        Bitmap image = mImages.get(i);
        imageGalleryAdapterViewHolder.mImageView.setImageBitmap(image);
        imageGalleryAdapterViewHolder.mImageCheck.setVisibility(View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        if(mImages == null) {
            return 0;
        }
        return mImages.size();
    }

    public void setImages(List<Bitmap> images) {
        this.mImages = images;
        notifyDataSetChanged();
    }
    public class ImageGalleryAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private ImageView mImageView;
        private ImageView mImageCheck;

        public ImageGalleryAdapterViewHolder(View view) {
            super(view);
            mImageView = (ImageView) view.findViewById(R.id.imageView);
            mImageCheck = (ImageView) view.findViewById(R.id.checkImage);

            mImageView.setOnClickListener(this);
            mImageView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(v.getContext(), "on click", Toast.LENGTH_LONG).show();
            int adapterPosition = getAdapterPosition();
            Bitmap selectedImage = mImages.get(adapterPosition);
            mClickHandler.onClick(selectedImage, this);
        }

        @Override
        public boolean onLongClick(View v) {
            int adapterPosition = getAdapterPosition();
            Bitmap selectedImage = mImages.get(adapterPosition);
            mClickHandler.onLongClick(selectedImage);
            mImageCheck.setVisibility(View.VISIBLE);
            return true;
        }

        public void setSelected(boolean isSelected) {
            if(isSelected) {
                mImageCheck.setVisibility(View.VISIBLE);
            } else {
                mImageCheck.setVisibility(View.INVISIBLE);
            }
        }
    }
}
