package com.example.mctest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mctest.databinding.ActivityProfileBinding;
import com.example.mctest.utilities.ProgressDialogUtil;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileActivity extends AppCompatActivity {

    private ActivityProfileBinding mBinding;
    String url;
    private int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        url = getString(R.string.url)+ getString(R.string.user_uri);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile);

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.session_preferences), Context.MODE_PRIVATE);
        userId = sharedPreferences.getInt(getString(R.string.userId), 0);

        populateUserInfo();
    }

    private void populateUserInfo() {
        ProgressDialogUtil.start(this);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url + "/" + userId, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            mBinding.profileEmailValueTF.setText(response.getString(getString(R.string.email_dto)));
                            mBinding.profilePasswordValueTF.setText(response.getString(getString(R.string.password_dto)));
                            ProgressDialogUtil.stop(ProfileActivity.this);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ProfileActivity.this,"Unknown user", Toast.LENGTH_SHORT).show();
                        Log.d("Error.Response", "test");
                    }
                }
        );
        MyRequestQueue.add(getRequest);
    }
}
