package com.example.mctest.handler;

import android.graphics.Bitmap;
import android.view.Menu;
import android.view.MenuItem;

import com.example.mctest.utilities.ImageGalleryAdapter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ImageGalleryHandler  implements ImageAdapterOnClickHandler{
    private List<Bitmap> mImages;
    private ImageGalleryAdapter mImageGalleryAdapter;
    private Menu menu;
    private Set<Bitmap> selectedImages;
    private boolean MULTIPLE_SELECT_MODE_ENABLED = false;

    public ImageGalleryHandler(List<Bitmap> images) {
        this.mImages = images;
    }

    @Override
    public void setImageGalleryAdapter(ImageGalleryAdapter mImageGalleryAdapter) {
        this.mImageGalleryAdapter = mImageGalleryAdapter;
    }

    @Override
    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    @Override
    public void onClick(Bitmap selectedImage, ImageGalleryAdapter.ImageGalleryAdapterViewHolder imageGalleryAdapterViewHolder) {
        if(MULTIPLE_SELECT_MODE_ENABLED) {
            boolean isSelected = selectedImages.contains(selectedImage);
            imageGalleryAdapterViewHolder.setSelected(!isSelected);
            if(isSelected) {
                selectedImages.remove(selectedImage);
            } else {
                selectedImages.add(selectedImage);
            }
            return;
        }
        mImages.remove(selectedImage);
        mImageGalleryAdapter.setImages(mImages);
    }

    @Override
    public void onLongClick(Bitmap selectedImage) {
        MULTIPLE_SELECT_MODE_ENABLED = true;

        selectedImages = new HashSet<>();
        selectedImages.add(selectedImage);

        MenuItem item = menu.getItem(0);
        item.setVisible(true);
    }

    @Override
    public void removeImages() {
        for(Bitmap image : selectedImages) {
            mImages.remove(image);
        }
        mImageGalleryAdapter.setImages(mImages);
        MULTIPLE_SELECT_MODE_ENABLED = false;
    }
}
