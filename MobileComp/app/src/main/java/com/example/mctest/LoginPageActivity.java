package com.example.mctest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mctest.databinding.ActivityLoginPageBinding;
import com.example.mctest.utilities.ProgressDialogUtil;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginPageActivity extends AppCompatActivity implements SensorEventListener {

    private ActivityLoginPageBinding mBinding;
    private String url;

    private SensorManager senSensorManager;
    private Sensor senAccelerometer;

    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private static final int SHAKE_THRESHOLD = 1400;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = getString(R.string.url) + getString(R.string.user_uri) + "/login";

        setContentView(R.layout.activity_login_page);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login_page);

        mBinding.loginLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        mBinding.loginRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRegisterActivity();
            }
        });

        openWiFi();

        activateShakeDetection();
    }

    private void activateShakeDetection() {
        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void openWiFi() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        Log.d("login", "" + isConnected);
        if (isConnected == false) {
            WifiManager wmgr = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            wmgr.setWifiEnabled(true);
            Toast.makeText(this, "WiFi is needed to be turned on", Toast.LENGTH_SHORT).show();
        }
    }

    private void openRegisterActivity() {
        Intent intentMain = new Intent(LoginPageActivity.this,
                RegisterActivity.class);
        LoginPageActivity.this.startActivity(intentMain);
    }

    private void login() {
        ProgressDialogUtil.start(LoginPageActivity.this);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(this);
        JSONObject postparams = new JSONObject();
        try {
            postparams.put("email", mBinding.loginEmailTF.getText().toString());
            postparams.put("password", mBinding.loginPasswordTF.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, url, postparams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogUtil.stop(LoginPageActivity.this);
                        Intent intentMain = new Intent(LoginPageActivity.this,
                                MainActivity.class);
                        SharedPreferences sharedPref = LoginPageActivity.this.getSharedPreferences(getString(R.string.session_preferences), Context.MODE_PRIVATE);
                        try {
                            sharedPref.edit().putInt(getString(R.string.userId), response.getInt(getString(R.string.id))).commit();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        LoginPageActivity.this.startActivity(intentMain);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ProgressDialogUtil.stop(LoginPageActivity.this);
                        Toast.makeText(LoginPageActivity.this, "Unknown user", Toast.LENGTH_SHORT).show();
                        error.printStackTrace();
                        Log.d("Error.Response", "test");
                    }
                }
        );
        MyRequestQueue.add(getRequest);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            long curTime = System.currentTimeMillis();

            if ((curTime - lastUpdate) > 100) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                float speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;

                if (speed > SHAKE_THRESHOLD) {
                    Toast.makeText(LoginPageActivity.this, getString(R.string.shake_message), Toast.LENGTH_SHORT).show();
                }

                last_x = x;
                last_y = y;
                last_z = z;
            }

        }
    }

    protected void onPause() {
        super.onPause();
        senSensorManager.unregisterListener(this);
    }

    protected void onResume() {
        super.onResume();
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
