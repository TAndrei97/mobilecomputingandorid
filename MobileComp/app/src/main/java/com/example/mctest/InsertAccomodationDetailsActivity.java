package com.example.mctest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mctest.databinding.ActivityInsertAccomodationDetailsBinding;
import com.example.mctest.handler.ImageAdapterOnClickHandler;
import com.example.mctest.handler.ImageGalleryHandler;
import com.example.mctest.model.Accomodation;
import com.example.mctest.model.Room;
import com.example.mctest.utilities.ImageGalleryAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class InsertAccomodationDetailsActivity extends AppCompatActivity {

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int SELECT_IMAGE = 2;

    private ActivityInsertAccomodationDetailsBinding mBinding = null;
    private ImageGalleryAdapter mImageGalleryAdapter;
    private List<Bitmap> mImages = new ArrayList<>();
    private ImageAdapterOnClickHandler mImageGalleryHandler;
    private Menu mMenu;
    private int userId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_accomodation_details);

        mImageGalleryHandler = new ImageGalleryHandler(mImages);
        mImageGalleryAdapter = new ImageGalleryAdapter(mImageGalleryHandler);
        mImageGalleryHandler.setImageGalleryAdapter(mImageGalleryAdapter);

        mImageGalleryHandler.setMenu(mMenu);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_insert_accomodation_details);


        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mBinding.recyclerView.setLayoutManager(layoutManager);
        mBinding.recyclerView.setAdapter(mImageGalleryAdapter);
        mBinding.recyclerView.setNestedScrollingEnabled(false);
        mBinding.takeAPhotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });

        mBinding.galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
            }
        });

        mBinding.addDetailSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveAccomodation();
            }
        });

    }



    private void saveAccomodation() {
        Bundle extras = getIntent().getExtras();
        String name = null;
        String description = null;
        String phone = null;
        String address = null;
        List<Room> rooms = new ArrayList<>();
        rooms.add(new Room(1,1,1));
        if (extras != null) {
            name = extras.getString(getString(R.string.accomodation_name));
            description = extras.getString(getString(R.string.accomodation_description));
            phone = extras.getString(getString(R.string.accomodation_phone));
            address = extras.getString(getString(R.string.accomodation_address));
        }
        Accomodation accomodation = new Accomodation(0,name, description, phone, address);

        RequestQueue MyRequestQueue = Volley.newRequestQueue(this);
        JSONObject postparams = new JSONObject();
        try {
            postparams.put("name", accomodation.getName());
            postparams.put("description", accomodation.getDescription());
            postparams.put("phone", accomodation.getPhone());
            postparams.put("address", accomodation.getAddress());

            postparams.put("rooms", rooms);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.session_preferences), Context.MODE_PRIVATE);
        userId = sharedPreferences.getInt(getString(R.string.userId), 0);

        String url = getString(R.string.url) + getString(R.string.accomodation_uri) + "/" + userId;

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, url, postparams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(InsertAccomodationDetailsActivity.this, "Saved", Toast.LENGTH_SHORT).show();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(InsertAccomodationDetailsActivity.this, "Error", Toast.LENGTH_SHORT).show();
                        error.printStackTrace();
                        Log.d("Error.Response", "test");
                    }
                }
        );
        MyRequestQueue.add(getRequest);
    }

      @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        List<Bitmap> images = new ArrayList<>();
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap image = (Bitmap) extras.get("data");
            images.add(image);
        } else if (requestCode == SELECT_IMAGE && resultCode == RESULT_OK) {
            try {
                if (data.getClipData() != null) {
                    int count = data.getClipData().getItemCount();
                    for (int i = 0; i < count; i++) {
                        Bitmap image = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getClipData().getItemAt(i).getUri());
                        images.add(image);
                    }
                } else if (data.getData() != null) {

                    Bitmap image = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                    images.add(image);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for (Bitmap image : images) {
            if(image == null) {
                continue;
            }
            mImages.add(image);
            mImageGalleryAdapter.setImages(mImages);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.manage_images, menu);
        MenuItem item = menu.getItem(0);
        item.setVisible(false);
        mImageGalleryHandler.setMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.remove_photo) {
            Toast toast = Toast.makeText(this, "Remove", Toast.LENGTH_LONG);
            toast.show();
            mImageGalleryHandler.removeImages();
            item.setVisible(false);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
