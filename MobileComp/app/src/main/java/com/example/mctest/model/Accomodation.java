package com.example.mctest.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Accomodation {

    private Integer id;
    private String name;
    private String description;
    private String phone;
    private String address;
}
